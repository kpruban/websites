package main

import (
	"encoding/json"
	"html/template"
	"log"
	"math/rand"
	"net/http"
)

type Student struct {
	Imie     string `json:"imie"`
	Nazwisko string `json:"nazwisko"`
	Indeks   int    `json:"indeks"`
	Email    string `json:"-"`
}

var studenci []Student = []Student{
	{"Jan", "Kowalski", 12345, "test@test"},
	{"Marek", "Nowak", 30000, "to@tamto"},
	{"Anna", "Zdyb", 23232, "anna@zdyb"},
}

func handleTest(w http.ResponseWriter, r *http.Request) {
	tmpl, _ := template.ParseFiles("dane.html")
	tmpl.Execute(w, studenci)
}

func handleJSON(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(studenci)
}

func handleRandomPage(w http.ResponseWriter, r *http.Request) {
	tmpFiles := []string{"page1.html", "page2.html", "page3.html"}
	randomIndex := rand.Intn(len(tmpFiles))
	randomPage := tmpFiles[randomIndex]
	tmpl, _ := template.ParseFiles(randomPage)
	tmpl.Execute(w, nil)
}

func main() {
	http.HandleFunc("/zad3/", handleRandomPage)
	http.HandleFunc("/test/", handleTest)
	http.HandleFunc("/json/", handleJSON)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
